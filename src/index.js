import Resolver from '@forge/resolver';
import {storage} from '@forge/api';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return 'Hello, world!';
});

resolver.define('storeData', (req) => {
  console.log(req);
  storage.set('username', req['payload']['formData']['username']);
  return 'Hello, world!';
});

resolver.define('showData', async (req) => {
  const username = await storage.get('username');
  return username;
});

export const handler = resolver.getDefinitions();
