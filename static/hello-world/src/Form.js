import React, { Fragment } from "react";

import Button from "@atlaskit/button/standard-button";
import TextField from "@atlaskit/textfield";
import { invoke } from "@forge/bridge";

import Form, {
  ErrorMessage,
  Field,
  FormFooter,
  FormHeader,
  HelperMessage,
} from "@atlaskit/form";

const FormDefaultExample = () => {
  return (
    <div>
      <Form
        onSubmit={async (data) => {
          console.log("form data", data);
          invoke("storeData", { formData: data }).then(() => {
            invoke("showData").then((res) => {
              console.log(res);
            });
          });
          await new Promise((resolve) => setTimeout(resolve, 2000));
          return data.username === "error" ? { username: "IN_USE" } : undefined;
        }}
      >
        {({ formProps, submitting }) => (
          <form {...formProps}>
            <FormHeader
              title="Trainee program"
              description="Custom UI lesson"
            />
            <Field aria-required={true} name="username" label="Username">
              {({ fieldProps, error }) => (
                <Fragment>
                  <TextField autoComplete="off" {...fieldProps} />
                  {!error && (
                    <HelperMessage>
                      You can use letters, numbers and periods.
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      This username is already in use, try another one.
                    </ErrorMessage>
                  )}
                </Fragment>
              )}
            </Field>
            <FormFooter>
              <Button type="submit" appearance="primary" isLoading={submitting}>
                Submit
              </Button>
            </FormFooter>
          </form>
        )}
      </Form>
    </div>
  );
};

export default FormDefaultExample;
